import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;



public class RankSeverity {

private static FileWriter file;
  public static void main(String[] args) {
    JSONParser jsonParser = new JSONParser();
    try (FileReader reader = new FileReader(args[0]))
       {
          Object obj = jsonParser.parse(reader);
          JSONArray cases = new JSONArray();
          cases.add(obj);
       
        List<Integer> totalCases = new ArrayList<Integer>();
        List<Integer> reportedCases = new ArrayList<Integer>();
        JSONObject h = (JSONObject) cases.get(0);
        JSONObject newJson = new JSONObject();
        for(int i = 0; i < h.size(); i++) {
            JSONObject article = (JSONObject) h.get(Integer.toString(i));
            parseEachObject(article, totalCases, reportedCases);
        }
     
      
       List<Integer> rank = getRanksArray(totalCases);
       for(int i = 0; i < rank.size(); i++) {
           JSONObject article = (JSONObject) h.get(Integer.toString(i));
           article.put("Total Cases", totalCases.get(i));
           article.put("Reported Cases", reportedCases.get(i));
           article.put("Severity Rank", rank.get(i));
           newJson.put(Integer.toString(i), article);
       }
       
       file = new FileWriter("output.json");
       file.write(newJson.toJSONString());
   
       file.close();
       
       }
       catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
  }

  public static void parseEachObject(JSONObject article, List<Integer> totalCases, List<Integer> reportedCases) {

    String text = (String) article.get("text");
    Pattern p = Pattern.compile("<CASE>(\\S+)</CASE>");
    Matcher m = p.matcher(text);
    int numCase = 0;
    int totalCase = 0;
     while (m.find()) {
       numCase++;
       String substring = text.substring( m.start(), m.end());
       Pattern p2 = Pattern.compile("-?\\d+");
       Matcher m2 = p2.matcher(substring);
       while(m2.find()){
         totalCase = totalCase + Integer.parseInt(m2.group());
       }
     }
     totalCases.add(totalCase);
     reportedCases.add(numCase);
  }

  public static List<Integer> getRanksArray(List<Integer> array) {
      List<Integer> result = new ArrayList<Integer>();

      for (int i = 0; i < array.size(); i++) {
          int count = 0;
          for (int j = 0; j < array.size(); j++) {
              if (array.get(j) > array.get(i)) {
                  count++;
              }
          }
          result.add(count + 1);
      }
      return result;
  }

}

