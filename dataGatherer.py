import requests
import random
import nltk
import operator
import spacy
import re
import geotext
import googlemaps
import key
import csv
import pycountry
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from datetime import date
import logging
import json

#pipreqs ./
#nltk.download()
#python -m spacy download en_core_web_sm

#average GIS coordinate location?
#time period/duration of data collection?

class caseRecord:

    def __init__(self, cases="", location="", date="", startDate="", endDate = "", datePublished=""):
        self.cases = cases
        self.location = location
        self.date = date
        self.start = startDate
        self.end = endDate
        self.datePublished = datePublished

    def __repr__(self):
        return self.location + "|" + self.cases + "|" + self.date + " (" + self.start + "-" + self.end + ")"

class formattedRecord:

    def __init__(self, country="", city="", cases=0, week=0, startWeek=0, endWeek=0, year=0, lat=0.0, long=0.0):
        self.country = country
        self.city = city
        self.cases = cases
        self.week = week
        self.startWeek = startWeek
        self.endWeek = endWeek
        self.year = year
        self.lat = lat
        self.long = long

    def __repr__(self):
        return "{},{},{},{},{},{},{},{},{}".format(self.city, self.country, self.cases, self.week, self.startWeek, self.endWeek, self.year, self.lat, self.long)

class promedRecord:

    def __init__(self, location="", archiveNum="", text="", datePublished="", source=""):

        self.location = location
        self.archiveNum = archiveNum
        self.text = text
        self.datePublished = datePublished
        self.source = source

    def __repr__(self):
        return self.location + ", " + self.archiveNum + ", " + self.datePublished + ", " + self.text + ", " + self.source

    def to_dict(self):
        return {"location": self.location, "archiveNumber": self.archiveNum, "text": self.text,
                "datePublished": self.datePublished, "source": self.source}

class taggedData:
    def __init__(self, promedRec=None, taggedText="", numCases=0, reportedCases=0, severityRank=0):
        self.promedRecord = promedRec
        self.taggedText = taggedText
        self.numCases = numCases
        self.reportedCases = reportedCases
        self.severityRank = severityRank

    def to_dict(self):
        return {"promedRecord": self.promedRecord.to_dict(), "taggedText":self.taggedText}


PROMED = "https://promedmail.org/wp-admin/admin-ajax.php"
GOOGLE = "https://maps.googleapis.com/maps/api/geocode/json"
stop_words = set(stopwords.words('english'))
common_words = set([line.rstrip() for line in open("keyWords.txt")])
gmaps = googlemaps.Client(key=key.APIKEY)
cachedLocationLookups = {}
LABEL = ["DATE", "LOCATION", "CASE"]

nlp = spacy.blank('en')

weekPattern = re.compile('(week(s*)\s\d+)')
weekNumberPattern = re.compile('(?<!\w)\d+')
cumulativePattern = re.compile('(cumulative)*\stotal\sof\s((\d+\s*)+)\s(\w\s*)*cases|total(\snumber)?\sof(\scases)?\s(\w\s*)+(\d+\s*)+\scases|(cumulative\s)*total\snumber\sof\s(\w\s*)*\scases\s(\w\s*)*\sis\s(\d+\s*)+')
datePattern = re.compile('(\d+\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|January|February|March|April|June|July|August|Semptember|October|November|December)\s\d+)|(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|January|February|March|April|June|July|August|Semptember|October|November|December)(\s\d+)*\s\d+|(beginning\sof\s\d+)|during\s(\d+)')
datePattern2 = re.compile('this week|the week under review|this year|the week in review')
casesPattern = re.compile('((\d+\s*\d*)+\s(new|confirmed|suspected|reported|new\ssuspected)\s(cases|case))|((\d+\s*\d*)+\s(cases|case))|((\d+\s*\d*)+\s(new|confirmed|suspected|reported|new\ssuspected)\scholera\scases)|((\d+\s*\d*)+\s(\snew|confirmed|suspected|reported|new\ssuspected)*\scholera\s*\/\s*AWD\scases)|cases\s(\w\s*)*\sis\s(\d+\s*)+')
deathsPattern = re.compile('((\d+\s*\d*)+(new|confirmed|suspected|deported|new\ssuspected)\s(deaths|death))|((\d+\s*\d*)+\s(deaths|death))')
locPattern = re.compile('((include|include;|includes:|including)\s(\w+,\s|and\s\w*)+)|\w*\s(region|district)')
numberPattern = re.compile('\(\d+.\d+%;\s\d+\)|\(\d+\)')
firstCheckPattern = re.compile('(\S+\s\((\d+\s*)+\))|(\S+\s\(\d+\.\d+%;\s(\d+\s*)+\))')
secondCheckPattern = re.compile('(\d+\s*)+\sin\s\w+')
lastKnownLocationPattern = re.compile('\S+:')
caseExtractorPattern = re.compile('(\d+\s\d*(?!Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Dec))+')
recordSplittingPattern = re.compile('\[(\d|10|11|12)\]\s\w+.+')


def break_removal(html_data):
    split = html_data.split('Source:')

    for i in range(1, len(split)):
        first = split[i].split('</a>', 1)  # Do this so we don't remove the <br /> between publisher and URL

        #sometimes the source is not an anchor tag so we have to split in a different way, by <br /> in this case
        if len(first) < 2:
            first = split[i].split('<br />', 2)

        current = first[len(first) - 1].split('--')  # Do this so we don't remove the <br /> after the body text
        # replace double break
        current[0] = current[0].replace("<br /><br />", "\ndoublebreak\n")
        current[0] = current[0].replace("<br /> <br />", "\ndoublebreak\n")
        # replace single break
        current[0] = current[0].replace('<br />', ' ')
        # add double break back in
        current[0] = current[0].replace("\ndoublebreak\n", "<br /><br />")
        new = "--"  # temp variable for joining array
        new = new.join(current)  # join the array
        split[i] = first[0] + "</a>" + new  # replace old text with processed text

    processed = "Source:"
    processed = processed.join(split)
    return processed

def getSingleProMedData(idNumber, req):
    data = {
        "action" : "get_latest_post_data",
        "alertId" : idNumber,
    }
    headers = {
        'user-agent' : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"
    }

    try:
        r = req.post(PROMED, data, headers=headers)
        htmlData = r.json()["post"]

        # preprocess of htmlData
        htmlData = break_removal(htmlData)

        soup = BeautifulSoup(htmlData, 'html.parser')
        # soup = BeautifulSoup(processed, 'html.parser')
        proRec = promedRecord()
        dataSet = list(soup.stripped_strings)
        data = ""
        allRecords = []
        foundSource = False
        nextParagraphSource = False

        locCheck = geotext.GeoText(dataSet[5])
        if len(locCheck.countries) > 0:
            for newLoc in locCheck.countries:
                proRec.location = newLoc
                break
        else:
            locParse = dataSet[5].split(':')
            if len(locParse) > 1:
                proRec.location = locParse[1]

        proRec.archiveNum = dataSet[7]
        #print(dataSet[3])

        for paragraph in dataSet:
            commonWordCounter = 0

            if nextParagraphSource and not foundSource and paragraph.startswith("http"):
                proRec.source = paragraph
                foundSource = True

            if paragraph.startswith("Date:"):
                dateCheck = re.search(datePattern, paragraph)
                if dateCheck is not None:
                    date = dateCheck.group()
                    proRec.datePublished = date

            elif paragraph.startswith("Source"):
                nextParagraphSource = True

            endRecord = re.match(recordSplittingPattern, paragraph)
            if endRecord is not None:
                if len(data) > 0:
                    proRec.text = data
                    allRecords.append(proRec)
                    tempArchive = proRec.archiveNum

                    proRec = promedRecord()
                    proRec.archiveNum = tempArchive
                    foundSource = False
                    nextParagraphSource = False
                    data = ""

                locCheck = geotext.GeoText(endRecord.group())
                if len(locCheck.countries) > 0:
                    for newLoc in locCheck.countries:
                        proRec.location = newLoc
                        break
                else:
                    locParse = endRecord.group().split(':')
                    if len(locParse) > 1:
                        proRec.location = endRecord.group()

                continue

            words = paragraph.split(" ")
            for word in words:
                newWord = re.sub(r'\W+', '', word)
                if newWord.lower() in common_words and len(newWord) > 0:
                    commonWordCounter += 1
                    if commonWordCounter >= 2:
                        data = data + paragraph + " "
                        break

        proRec.text = data
        allRecords.append(proRec)
        return allRecords

    except json.decoder.JSONDecodeError:
        print("failure to parse json. Skipping")
    except:
        print("unknown execption met. Skipping record")

def getAllProMedData():
    page = 1
    resultsDict = {}
    counter = 0
    data = {
        "action" : "get_promed_search_content",
        "query[0][name]" : "pagenum",
        "query[0][value]" : page - 1,
        "query[1][name]" : "kwby1",
        "query[1][value]" : "summary",
        "query[2][name]" : "search",
        "query[2][value]" : "cholera",
        "query[3][name]" : "date1",
        "query[3][value]" : "01/01/2010",
        #"query[3][value]": "08/18/2012",
        "query[4][name]" : "date2",
        #"query[4][value]": "01/01/2013",
        "query[4][value]" : "01/01/2020",
        "query[5][name]" : "feed_id",
        "query[5][value]" : 1,
        "query[6][name]" : "submit",
        "query[6][value]" : "next",
    }
    headers = {
        'user-agent' : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36",
        'Connection' : 'Keep-Alive',
    }
    req = requests.Session()

    check = ""
    print("Starting data collection")
    while check != "No Results":
        r = req.post(PROMED, data, headers=headers)
        htmlData = r.json()["results"]
        soup = BeautifulSoup(htmlData, 'html.parser')
        check = soup.get_text()
        aTags = soup.find_all("a")
        for tag in aTags:
            results = getSingleProMedData(tag['id'].strip("id"), req)
            if results is not None:
                for result in results:
                    resultsDict[counter] = result
                    counter += 1

        page += 1
        data["query[0][value]"] = page

    return resultsDict

def normalizeRecords(caseList=None, lastKnownLocation="", datePublished=""):
    finalList = []
    if caseList is None:
        return finalList

    for case in caseList:
        result = formattedRecord()
        result.cases = list(weekNumberPattern.finditer(case.cases))[0].group()

        splitDate = datePublished.split(' ')
        monthNumber = getMonth(splitDate[1])
        isoCalendarPublished = date(int(splitDate[2]), monthNumber, int(splitDate[0])).isocalendar()
        publishWeek = isoCalendarPublished[1]
        publishYear = isoCalendarPublished[0]

        result.year = publishYear

        if "this week" in case.date or "the week in review" in case.date or "the week under review" in case.date:
            result.week = publishWeek
        elif "beginning" in case.date:
            result.week = 1
            result.startWeek = 1
            result.endWeek = publishWeek
        elif "this year" in case.date:
            result.week = 1
            result.startWeek = 1
            result.endWeek = publishWeek
        elif "week" in case.date:
            result.week = list(weekNumberPattern.finditer(case.date))[0].group()
            caseDate = case.start.split(' ')
            if len(caseDate) == 3:
                monthNum = getMonth(caseDate[1])
                isoCalendarDate = date(int(caseDate[2]), monthNum, int(caseDate[0])).isocalendar()
                result.startWeek = isoCalendarDate[1]
            elif "week" in case.start:
                result.startWeek = list(weekNumberPattern.finditer(case.start))[0].group()

            caseDate = case.end.split(' ')
            if len(caseDate) == 3:
                monthNum = getMonth(caseDate[1])
                isoCalendarDate = date(int(caseDate[2]), monthNum, int(caseDate[0])).isocalendar()
                result.endWeek = isoCalendarDate[1]
            elif "week" in case.end:
                result.endWeek = list(weekNumberPattern.finditer(case.end))[0].group()

        else:
            caseDate = case.date.split(' ')
            monthNum = getMonth(caseDate[1])
            isoCalendarDate = date(int(caseDate[2]), monthNum, int(caseDate[0])).isocalendar()
            result.week = isoCalendarDate[1]

            caseDate = case.start.split(' ')
            if len(caseDate) == 3:
                monthNum = getMonth(caseDate[1])
                isoCalendarDate = date(int(caseDate[2]), monthNum, int(caseDate[0])).isocalendar()
                result.startWeek = isoCalendarDate[1]
            elif "week" in case.start:
                result.startWeek = list(weekNumberPattern.finditer(case.start))[0].group()

            caseDate = case.end.split(' ')
            if len(caseDate) == 3:
                monthNum = getMonth(caseDate[1])
                isoCalendarDate = date(int(caseDate[2]), monthNum, int(caseDate[0])).isocalendar()
                result.endWeek = isoCalendarDate[1]
            elif "week" in case.end:
                result.endWeek = list(weekNumberPattern.finditer(case.end))[0].group()

        geocode_result = None
        alpha2 = ""

        if case.location in cachedLocationLookups:
            geocode_result = cachedLocationLookups[case.location]
        else:
            if case.location != "":
                try:
                    alpha2 = pycountry.countries.search_fuzzy(case.location)[0].alpha_2
                except LookupError:
                    print("lookup error for location. Trying last known location")
            if lastKnownLocation != "" and alpha2 == "":
                try:
                    alpha2 = pycountry.countries.search_fuzzy(lastKnownLocation)[0].alpha_2
                except LookupError:
                    print("lookup error for location. Cannot find a 2 letter alpha for region")

            components = {'country': alpha2}
            geocode_result = gmaps.geocode(case.location, components=components)

        if len(geocode_result) > 0:
            cachedLocationLookups[case.location] = geocode_result

            res = geocode_result[0]
            result.lat = res['geometry']['location']['lat']
            result.long = res['geometry']['location']['lng']

            address = res['address_components']
            typeChosen = ""
            for part in address:
                if "administrative_area_level_2" in part['types']:
                    result.city = part['long_name']
                    typeChosen = "administrative_area_level_2"

                elif "administrative_area_level_1" in part['types']:
                    if typeChosen != "administrative_area_level_2":
                        result.city = part['long_name']
                        typeChosen = "administrative_area_level_1"

                elif "country" in part['types']:
                    result.country = part['long_name']

        #if we cannot find a match with no alpha2 code selected, then just return the information we have
        elif alpha2 == "":
            print("cannot find a match on google API")

        #otherwise, try removing the alpha2 component and doing a search to see if that was messing with results
        else:
            geocode_result = gmaps.geocode(case.location)
            if len(geocode_result) > 0:
                cachedLocationLookups[case.location] = geocode_result

                res = geocode_result[0]
                result.lat = res['geometry']['location']['lat']
                result.long = res['geometry']['location']['lng']

                address = res['address_components']
                typeChosen = ""
                for part in address:
                    if "administrative_area_level_2" in part['types']:
                        result.city = part['long_name']
                        typeChosen = "administrative_area_level_2"

                    elif "administrative_area_level_1" in part['types']:
                        if typeChosen != "administrative_area_level_2":
                            result.city = part['long_name']
                            typeChosen = "administrative_area_level_1"

                    elif "country" in part['types']:
                        result.country = part['long_name']

        finalList.append(result)
    return finalList

def getMonth(date=""):
    if date in 'January':
        return 1
    elif date in 'February':
        return 2
    elif date in 'March':
        return 3
    elif date in 'April':
        return 4
    elif date in 'May':
        return 5
    elif date in 'June':
        return 6
    elif date in 'July':
        return 7
    elif date in 'August':
        return 8
    elif date in 'September':
        return 9
    elif date in 'October':
        return 10
    elif date in 'November':
        return 11
    elif date in 'December':
        return 12

def generateRecords(cases=None, dates=None, locations = None, date="", lastKnownLocation = "", datePublished=""):
    allCases = []
    if cases is None or dates is None or locations is None:
        return

    if len(cases) == 1:
        #if len(dates) == 0 --> last known date
        if len(dates) == 0:
            if len(locations) == 0:
                if lastKnownLocation == "" or date == "":
                    return
                record = caseRecord(cases=cases[0].cases, location=lastKnownLocation, date=date, datePublished=datePublished)
                allCases.append(record)
            elif len(locations) == 1:
                record = caseRecord(cases=cases[0].cases, location=locations[0].location, date=date, datePublished=datePublished)
                allCases.append(record)
            elif len(locations) > 1:
                record = caseRecord(cases=cases[0].cases, location=locations[0].location, date=date, datePublished=datePublished)
                allCases.append(record)

        # elif dates == 1 --> match case to date
        elif len(dates) == 1:
            if len(locations) == 0:
                if lastKnownLocation == "" or date == "":
                    return
                record = caseRecord(cases=cases[0].cases, location=lastKnownLocation, date=dates[0].date, datePublished=datePublished)
                allCases.append(record)
            elif len(locations) == 1:
                record = caseRecord(cases=cases[0].cases, location=locations[0].location, date=dates[0].date, datePublished=datePublished)
                allCases.append(record)
            elif len(locations) > 1:
                record = caseRecord(cases=cases[0].cases, location=locations[0].location, date=dates[0].date, datePublished=datePublished)
                allCases.append(record)

        #elif dates == 2 --> match case to range
        elif len(dates) == 2:
            if len(locations) == 0:
                if lastKnownLocation == "" or date == "":
                    return
                record = caseRecord(cases=cases[0].cases, location=lastKnownLocation, date=dates[0].date, startDate=dates[0].date, endDate=dates[1].date, datePublished=datePublished)
                allCases.append(record)
            elif len(locations) == 1:
                record = caseRecord(cases=cases[0].cases, location=locations[0].location, date=dates[0].date, startDate=dates[0].date, endDate=dates[1].date, datePublished=datePublished)
                allCases.append(record)
            elif len(locations) > 1:
                record = caseRecord(cases=cases[0].cases, location=locations[0].location, date=dates[0].date, startDate=dates[0].date, endDate=dates[1].date, datePublished=datePublished)
                allCases.append(record)


            # if location == 0 --> last known location
            # if location == 1 --> 1 record
            # elif location > 1 --> multiple records

    else:
        for i in range(len(cases)):
            #NEED TO DEAL WITH LESS DATES THEN CASES
            if len(dates) == 0:
                if len(locations) == 0:
                    if lastKnownLocation == "" or date == "":
                        return
                    record = caseRecord(cases=cases[i].cases, location=lastKnownLocation, date=date, datePublished=datePublished)
                    allCases.append(record)
                elif len(locations) == 1:
                    record = caseRecord(cases=cases[i].cases, location=locations[0].location, date=date, datePublished=datePublished)
                    allCases.append(record)
                elif len(locations) > 1:
                    record = caseRecord(cases=cases[i].cases, location=locations[i].location, date=date, datePublished=datePublished)
                    allCases.append(record)

            elif len(dates) == 1:
                if len(locations) == 0:
                    if lastKnownLocation == "" or date == "":
                        return
                    record = caseRecord(cases=cases[i].cases, location=lastKnownLocation, date=dates[0].date, datePublished=datePublished)
                    allCases.append(record)
                elif len(locations) == 1:
                    record = caseRecord(cases=cases[i].cases, location=locations[0].location, date=dates[0].date, datePublished=datePublished)
                    allCases.append(record)
                elif len(locations) > 1:
                    record = caseRecord(cases=cases[i].cases, location=locations[i].location, date=dates[0].date, datePublished=datePublished)
                    allCases.append(record)

            elif len(dates) > 1:
                if len(locations) == 0:
                    if lastKnownLocation == "" or date == "":
                        return
                    record = caseRecord(cases=cases[i].cases, location=lastKnownLocation, date=dates[i].date, datePublished=datePublished)
                    allCases.append(record)
                elif len(locations) == 1:
                    record = caseRecord(cases=cases[i].cases, location=locations[0].location, date=dates[i].date, datePublished=datePublished)
                    allCases.append(record)
                elif len(locations) > 1:
                    record = caseRecord(cases=cases[i].cases, location=locations[i].location, date=dates[i].date, datePublished=datePublished)
                    allCases.append(record)

        # elif dates == 1 --> match case to date
            # if location == 0 --> last known location
            # if location == 1 --> cases matched to their date and set to same location
            # elif location > 1 --> cases matched to their date and location
        # elif dates == 2 --> match case to range
            # if location == 0 --> last known location
            # if location == 1 --> 1 record
            # elif location > 1 --> multiple records

    #return normalizeRecords(caseList=allCases, lastKnownLocation=lastKnownLocation, datePublished=datePublished)

def parseNormal(line="", date="", lastKnownLocation = "", datePublished=""):
    caseFound = False
    locFound = False
    dateFound = False
    casesFound = []
    locationsFound = []
    datesFound = []
    for match in casesPattern.finditer(line):
        case = caseRecord(cases=match.group())
        casesFound.append(case)
        caseFound = True

    # if we did not find a case, there is no point in continuing
    if caseFound:
        cumulativeData = cumulativePattern.finditer(line)
        tempData = list(cumulativeData)

        #if there is no cumulative data found in our function
        if len(tempData) == 0:

            #look for any locations in the text
            for locMatch in locPattern.finditer(line):
                loc = caseRecord(location=locMatch.group())
                locationsFound.append(loc)
                locFound = True

            #if we did not find any location in the regular expression, use GeoText to
            #try and match a city or a country . If we still cant find anything, use
            #the last know location. If there is no, last known location, dont use the record
            if not locFound:
                places = geotext.GeoText(line)
                if len(places.cities) > 0:
                    for city in places.cities:
                        locationsFound.append(caseRecord(location=city))

                if len(places.countries) > 0:
                    for country in places.countries:
                        locationsFound.append(caseRecord(location=country))

            for weekMatch in weekPattern.finditer(line):
                date = caseRecord(date=weekMatch.group())
                dateFound = True
                datesFound.append(date)

            if not dateFound:
                for dateMatch in datePattern.finditer(line):
                    date = caseRecord(date=dateMatch.group())
                    datesFound.append(date)
                    dateFound = True

            if not dateFound:
                for dateMatch in datePattern2.finditer(line):
                    date = caseRecord(date=dateMatch.group())
                    datesFound.append(date)

            return generateRecords(cases=casesFound, dates=datesFound, locations=locationsFound, date=date,
                            lastKnownLocation=lastKnownLocation, datePublished=datePublished)

        #only cumulative data has been reported
        elif len(tempData) == len(casesFound):
            # look for any locations in the text
            for locMatch in locPattern.finditer(line):
                loc = caseRecord(location=locMatch.group())
                locationsFound.append(loc)
                locFound = True

            # if we did not find any location in the regular expression, use GeoText to
            # try and match a city or a country . If we still cant find anything, skip
            # the record
            if not locFound:
                places = geotext.GeoText(line)
                if len(places.cities) > 0:
                    locFound = True
                    for city in places.cities:
                        locationsFound.append(caseRecord(location=city))

                if len(places.countries) > 0:
                    locFound = True
                    for country in places.countries:
                        locationsFound.append(caseRecord(location=country))

            for weekMatch in weekPattern.finditer(line):
                date = caseRecord(date=weekMatch.group())
                dateFound = True
                datesFound.append(date)

            if not dateFound:
                for dateMatch in datePattern.finditer(line):
                    date = caseRecord(date=dateMatch.group())
                    datesFound.append(date)
            return generateRecords(cases=casesFound, dates=datesFound, locations=locationsFound, date=date,
                            lastKnownLocation=lastKnownLocation, datePublished=datePublished)




        #there are both individual records as well as cumulative records
        else:
            # look for any locations in the text
            for locMatch in locPattern.finditer(line):
                loc = caseRecord(location=locMatch.group())
                locationsFound.append(loc)
                locFound = True

            # if we did not find any location in the regular expression, use GeoText to
            # try and match a city or a country . If we still cant find anything, skip
            # the record
            if not locFound:
                places = geotext.GeoText(line)
                if len(places.cities) > 0:
                    for city in places.cities:
                        locationsFound.append(caseRecord(location=city))

                if len(places.countries) > 0:
                    for country in places.countries:
                        locationsFound.append(caseRecord(location=country))

            for weekMatch in weekPattern.finditer(line):
                date = caseRecord(date=weekMatch.group())
                datesFound.append(date)


            for dateMatch in datePattern.finditer(line):
                date = caseRecord(date=dateMatch.group())
                datesFound.append(date)

            for dateMatch in datePattern2.finditer(line):
                date = caseRecord(date=dateMatch.group())
                datesFound.append(date)

            return generateRecords(cases=casesFound, dates=datesFound, locations=locationsFound, date=date,
                                   lastKnownLocation=lastKnownLocation, datePublished=datePublished)


    else:
        return []

def parseSpecial(line="", cases=None, dateFound=False, date="", lastKnownLocation="", datePublished=""):
    locationsFound = []
    locationsMissing = []
    finalCases = []
    allCases = []
    # go through all the cases we found and look for a city or country specified
    for case in cases:
        places = geotext.GeoText(case.location)
        if len(places.cities) > 0:
            locationsFound.append(places.cities[0])
            case.location = places.cities[0]
            finalCases.append(case)

        elif len(places.countries) > 0:
            locationsFound.append(places.countries[0])
            case.location = places.countries[0]
            finalCases.append(case)

        else:
            word = nlp(case.location)
            for token in word:
                if not token.pos_ == "NOUN":
                    locationsMissing.append(case)

    # once we are done, we grab all the places listed in the line we are analyzing. We then loop
    # through all the missing data we have and check to see if there is a country or city in the line
    # that we have not recorded. The first encounter we have of that will become the location of
    # the missing data and we add that new location to the list of locations we have found
    places = geotext.GeoText(line)
    for missingData in locationsMissing:
        skipCountry = False
        for city in places.cities:
            if city not in locationsFound:
                locationsFound.append(city)
                missingData.location = city
                finalCases.append(missingData)
                skipCountry = True
                break

        # if a city was not found, check the country
        if not skipCountry:
            for country in places.countries:
                if country not in locationsFound:
                    locationsFound.append(country)
                    missingData.location = country
                    finalCases.append(missingData)
                    break

    if len(finalCases) > 0:
        # TODO find a better method to get the date if the date is not found in the line. Maybe check if date gotten here is a number or actual words
        # if we have not found a final date, search through the line to see if we can find a DATE
        # tag that we can use for a date. Otherwise, use the last know date as the value. We also check
        # to see if the word is a pure number. If it is, then we can most likely discard the case
        # because it is a false positive
        if not dateFound:
            nlpLine = nlp(line)
            for word in nlpLine.ents:
                if word.label_ == "DATE" and not word.text.isdigit():
                    date = word.text
                    break

        for case in finalCases:
            case.date = date
            allCases.append(case)

    return normalizeRecords(caseList=allCases, lastKnownLocation=lastKnownLocation, datePublished=datePublished)

def analyzeData(data):

    firstSplit = data.split("\n", 1)
    datePublished = firstSplit[0]
    splitData = re.split("\.\s|\n", firstSplit[1])
    date = ""
    lastKnownLocation = ""
    allDocCases = []

    for line in splitData:
        dateFound = False
        firstCheckFound = False
        secondCheckFound = False
        fullCase = []

        for weekMatch in weekPattern.finditer(line):
            date = weekMatch.group()
            dateFound = True
            break

        if not dateFound:
            for dateMatch in datePattern.finditer(line):
                date = dateMatch.group()
                dateFound = True
                break

        #find the last known location stated in a sentence. Usually deonted by "LOCAITON:"
        for locMatch in lastKnownLocationPattern.finditer(line):
            places = geotext.GeoText(locMatch.group())
            for country in places.countries:
                lastKnownLocation = country
                break
            break

        # check for a pattern of LOCATION (131) or LOCATION (23.5%; 422) and if we can do that,
        # use that information instead of going through the other regular expressions
        for match in firstCheckPattern.finditer(line):
            text = match.group().replace("(", "")
            text = text.replace(")", "")
            splitText = text.split()
            parsedText = caseRecord(cases=splitText[len(splitText) - 1], location=splitText[0], date=date)
            fullCase.append(parsedText)
            firstCheckFound = True

        # check for the pattern of NUMBER in LOCATION. If we can find that pattern then dont
        # go through the other regular expressions
        for match in secondCheckPattern.finditer(line):
            text = match.group().split()
            parsedText = caseRecord(cases=text[0], location=text[len(text) - 1], date=date)
            fullCase.append(parsedText)
            secondCheckFound = True

        result = []
        if not firstCheckFound and not secondCheckFound:
            result = parseNormal(line, date, lastKnownLocation=lastKnownLocation, datePublished=datePublished)

        else:
            result = parseSpecial(line, fullCase, dateFound, date, lastKnownLocation=lastKnownLocation, datePublished=datePublished)

        if result is not None:
            for value in result:
                allDocCases.append(value)

    return allDocCases
#used to create a list of the most common words that appear in promed articles
#this will be used to create an expanded list of words that we can use to shorten
#the articles we need

def createSynonyms():
    mostCommon = {}
    data = getAllProMedData()
    for key, item in data.items():
        if item != None:
            splitItem = item.split(" ")
            for word in splitItem:
                word = word.lower()
                if word not in stop_words:
                    if word not in mostCommon.keys():
                        mostCommon[word] = 1
                    else:
                        mostCommon[word] += 1
    sorted_d = sorted(mostCommon.items(), key=operator.itemgetter(1), reverse=True)
    print(sorted_d)

#expands on the list of words that is defined in keywords.txt
def expandSynonyms():
    global common_words
    synonymList = list()
    for word in common_words:
        synset = nltk.corpus.wordnet.synsets(word)
        if len(synset) == 0 or len(synset[0].lemma_names()) == 0:
            continue
        for lemma in synset:
            synonymList += lemma.lemma_names()

    for word in synonymList:
        common_words.add(word)

def convert_dataturks_to_spacy(dataturks_JSON_FilePath):
    try:
        training_data = []
        lines=[]
        with open(dataturks_JSON_FilePath, 'r') as f:
            lines = f.readlines()

        for line in lines:
            data = json.loads(line)
            text = data['content']
            entities = []

            if data['annotation'] is None:
                continue

            for annotation in data['annotation']:
                #only a single point in text annotation.
                point = annotation['points'][0]
                labels = annotation['label']
                # handle both list of labels or a single label.
                if not isinstance(labels, list):
                    labels = [labels]

                for label in labels:
                    #dataturks indices are both inclusive [start, end] but spacy is not [start, end)
                    entities.append((point['start'], point['end'] + 1 ,label))


            training_data.append((text, {"entities" : entities}))

        return training_data
    except Exception as e:
        logging.exception("Unable to process " + dataturks_JSON_FilePath + "\n" + "error = " + str(e))
        return None

def train_spacy():
    TRAIN_DATA = convert_dataturks_to_spacy("train.json")
    # create the built-in pipeline components and add them to the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner, last=True)

    # add labels
    for _, annotations in TRAIN_DATA:
        for ent in annotations.get('entities'):
            ner.add_label(ent[2])

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  # only train NER
        optimizer = nlp.begin_training()
        print("Starting spacy training")
        for itn in range(25):
            random.shuffle(TRAIN_DATA)
            losses = {}
            for text, annotations in TRAIN_DATA:
                nlp.update(
                    [text],  # batch of texts
                    [annotations],  # batch of annotations
                    drop=0.2,  # dropout - make it harder to memorise data
                    sgd=optimizer,  # callable to update weights
                    losses=losses)

def insert_tag(text, start, end, tag):
    return text[:start] + "<" + tag + ">" + text[start:end] + "<\\" + tag + ">" + text[end:]

def tag_records(record):
    taggedRecord = taggedData()
    taggedRecord.promedRecord = record
    doc = nlp(record.text)
    offset = 0
    tempText = record.text
    for ent in doc.ents:
        tempText = insert_tag(tempText, ent.start_char + offset, ent.end_char + offset, ent.label_)
        offset = offset + (2 * len(ent.label_)) + 5

        if ent.label_ == "CASE":
            numList = re.findall('(\d+\s*)+', ent.string)
            for num in numList:
                taggedRecord.numCases += int(num)
                taggedRecord.reportedCases += 1

    taggedRecord.taggedText = tempText
    return taggedRecord

def export_CSV(rankList=None):
    with open('output.csv', 'w', errors='ignore', newline='') as rankFile:
        writer = csv.writer(rankFile)
        writer.writerow(("Archive Number", "Date Published", "Location", "Source", "Reported Cases", "Number Cases", "Severity Rank", "Tagged Text", "Text"))
        for line in rankList:
            writer.writerow((line.promedRecord.archiveNum, line.promedRecord.datePublished, line.promedRecord.location, line.promedRecord.source, line.reportedCases, line.numCases, line.severityRank, line.taggedText, line.promedRecord.text))

def main():
    train_spacy()
    expandSynonyms()
    gatheredData = getAllProMedData()

    taggedData = []
    counter = 0
    rankList=[]
    for data in gatheredData.values():
        taggedData.append(tag_records(data))
        counter += 1

    rankList = sorted(taggedData, key=lambda x: x.numCases, reverse=True)
    i = 0
    for i in range(i, len(rankList)):
        rankList[i].severityRank = i + 1

    export_CSV(rankList)
    print("done")

if __name__ == '__main__':
    main()
