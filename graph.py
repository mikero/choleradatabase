import json
import io
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt

pd.set_option("display.max_rows", None, "display.max_columns", None)


with io.open('condensedData.json', mode='r', encoding="utf-8") as read_file:
    data = json.load(read_file)


dates = []

df = pd.DataFrame(columns=["date", "location"]).astype({"date":"datetime64", "location":str})


for i in data.keys():
    

    date_str = data[i]['datePublished']
    if date_str:  # check for a date
        date_list = date_str.split(" ")

        if len(date_list) == 3:  # check if the date has 3 fields
            if len(date_list[2]) > 4:  # error correction for the year
                    date_list[2] = date_list[2][0:4]
                    date_str = " ".join(date_list)

            if str.isalpha(date_list[0]):  # check if the first field is texual month
                if int(date_list[1]) > 31:  # error correction for the day
                    date_list[1] = date_list[1][0:2]
                    date_str = " ".join(date_list)
                if len(date_list[0]) == 3:  # check the month format (3 letter vs full)
                    date = datetime.strptime(date_str, "%b %d %Y")
                else:
                    date = datetime.strptime(date_str, "%B %d %Y")
            
            else:
                if int(date_list[0]) > 31:  # error correction for the day
                    date_list[0] = date_list[0][0:2]
                    date_str = " ".join(date_list)
                if len(date_list[1]) == 3:
                    date = datetime.strptime(date_str, "%d %b %Y")
                else:
                    date = datetime.strptime(date_str, "%d %B %Y")
        
        else:  # we dont have a day
            if len(date_list[1]) == 2:  #check the year format (2 digit vs full)
                if len(date_list[0]) == 3:  # check the month format (3 letter vs full)
                    date = datetime.strptime("1 "+date_str, "%d %b %y")
                else:
                    date = datetime.strptime("1 "+date_str, "%d %B %y")
            else:
                if len(date_list[0]) == 3:  # check the month format (3 letter vs full)
                    date = datetime.strptime("1 "+date_str, "%d %b %Y")
                else:
                    date = datetime.strptime("1 "+date_str, "%d %B %Y")


        location = data[i]['location']

        df = df.append({'date': date, 'location': location}, ignore_index=True)
        


locations = {k: v for k, v in df.groupby("location").count().to_dict()["date"].items() if v > 1}.keys()
print(locations)


plt.figure(figsize=(70,35))
df["date"].groupby([df["date"].dt.year, df["date"].dt.week]).count().plot(kind="bar")
plt.title('global', fontsize=30)
plt.xlabel("year, week", fontsize=30)
plt.ylabel("# of reports", fontsize=30)
plt.savefig("plots/global-week")

plt.figure(figsize=(20,10))
df["date"].groupby([df["date"].dt.year, df["date"].dt.month]).count().plot(kind="bar")
plt.title('global', fontsize=30)
plt.xlabel("year, month", fontsize=30)
plt.ylabel("# of reports", fontsize=30)
plt.savefig("plots/global-month")

for location in locations:
    df["date"].loc[df["location"]==location].groupby([df["date"].dt.year, df["date"].dt.week]).count().plot(kind="bar")
    plt.title(location)
    plt.xlabel("year, week", fontsize=10)
    plt.ylabel("# of reports", fontsize=10)
    plt.savefig("plots/"+location+"-week")

    df["date"].loc[df["location"]==location].groupby([df["date"].dt.year, df["date"].dt.month]).count().plot(kind="bar")
    plt.title(location)
    plt.xlabel("year, month", fontsize=10)
    plt.ylabel("# of reports", fontsize=10)
    plt.savefig("plots/"+location+"-month")
