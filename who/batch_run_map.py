import make_map
from argparse import Namespace


# num of cases
make_map.main(Namespace(filename="out/cases/cases_2010_per_country.csv", fileoutput="images/cases_2010_per_country.png"))
make_map.main(Namespace(filename="out/cases/cases_2011_per_country.csv", fileoutput="images/cases_2011_per_country.png"))
make_map.main(Namespace(filename="out/cases/cases_2012_per_country.csv", fileoutput="images/cases_2012_per_country.png"))
make_map.main(Namespace(filename="out/cases/cases_2013_per_country.csv", fileoutput="images/cases_2013_per_country.png"))
make_map.main(Namespace(filename="out/cases/cases_2014_per_country.csv", fileoutput="images/cases_2014_per_country.png"))
make_map.main(Namespace(filename="out/cases/cases_2015_per_country.csv", fileoutput="images/cases_2015_per_country.png"))
make_map.main(Namespace(filename="out/cases/cases_2016_per_country.csv", fileoutput="images/cases_2016_per_country.png"))
