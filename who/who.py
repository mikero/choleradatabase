import argparse
import csv


# Class to represent countries
class Country:
    def __init__(self, name):
        self.total = None
        self.name = name
        self.cases = {}  # should be a dictionary made up of pairs (year, number_of_cases)
        self.total = 0


def main(args):
    # Retrieve parsed arguments
    filename = args.filename
    min_year = args.minyear
    max_year = args.maxyear
    num_countries = args.numcountries
    country = args.country
    breakdown = args.breakdown
    file_output = args.fileoutput

    countries = []

    # Read csv and extract data
    with open(filename) as file:
        reader = csv.reader(file, delimiter=',')
        next(reader)  # skip the header

        prev_country = ""  # to keep track of when to insert a new country

        # Iterate over file
        for row in reader:
            # Check if the year is between the desired range
            if min_year <= int(row[1]) <= max_year:
                # Check if there was a required country or we need all of them
                if (country is not None and country == row[0]) or country is None:
                    # Check if we need to insert a new country
                    if row[0] != prev_country:
                        countries.append(Country(row[0]))
                        prev_country = row[0]
                    countries[-1].cases[row[1]] = float(row[2])  # each row is a year and the num of cases
                    countries[-1].total += float(row[2])  # for each new year added, update the total for that country

    sorted_countries = []

    # Sort countries based on total cases (selection sort algorithm)
    while len(countries) > 0:
        max_value = 0
        for i in range(0, len(countries)):
            if countries[i].total >= max_value:
                max_pos = i
                max_value = countries[i].total
        sorted_countries.append(countries.pop(max_pos))

    sorted_countries = sorted_countries[0:num_countries]


    # Check if we need to write to file...
    if file_output:

        with open(file_output, "w+", newline='') as file:
            writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

            # Check if we need to display the year-by-year breakdown...
            if breakdown:
                writer.writerow(["Country", "Year", "Count"])
                for c in sorted_countries:
                    for year in c.cases.keys():
                        writer.writerow([c.name, year, str(c.cases[year])])
            # ... or the total
            else:
                writer.writerow(["Country", "Total"])
                for c in sorted_countries:
                    writer.writerow([c.name, str(c.total)])


    # ... or print to console
    else:

        # Check if we need to display the year-by-year breakdown...
        if breakdown:
            for c in sorted_countries:
                print(c.name + ":")
                for year in c.cases.keys():
                    print("\t" + year + ": " + str(c.cases[year]))
        # ... or the total
        else:
            for c in sorted_countries:
                print(c.name + ": " + str(c.total))


if __name__ == "__main__":
    # Parse CLI arguments
    parser = argparse.ArgumentParser(
        description="Filter and display the number of cholera cases over the years and across "
                    "the countries, extracted from the WHO dataset. This script creates "
                    "a list that orders countries from higher to lower number of cases")
    parser.add_argument("filename", help="path to the csv file containing the data")
    parser.add_argument("-m", "--minyear", help="minimum year (included)", default=0, type=int)
    parser.add_argument("-x", "--maxyear", help="maximum year (included)", default=10000, type=int)
    parser.add_argument("-n", "--numcountries", help="number of countries to display (from the top of the list)",
                        default=100000, type=int)
    parser.add_argument("-c", "--country",
                        help="search for one specific country (if not specified, all countries will be "
                             "taken into consideration when making the list)")
    parser.add_argument("-b", "--breakdown", help="for each country, display data year-by-year (if not set, years will "
                                                  "be added up)", action="store_true")
    parser.add_argument("-f", "--fileoutput",
                        help="send the output to a cvs file instead of printing it (specify name)")
    arguments = parser.parse_args()

    main(arguments)
