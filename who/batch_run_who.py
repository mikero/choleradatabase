import who
from argparse import Namespace


# num of cases
who.main(Namespace(filename="./resources/cases.csv", minyear=2010, maxyear=2016, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2010-2016_total_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2010, maxyear=2016, numcountries=None, country=None,
                   breakdown=True, fileoutput="./out/cases/cases_2010-2016_year_breakdown_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2010, maxyear=2010, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2010_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2011, maxyear=2011, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2011_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2012, maxyear=2012, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2012_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2013, maxyear=2013, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2013_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2014, maxyear=2014, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2014_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2015, maxyear=2015, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2015_per_country.csv"))
who.main(Namespace(filename="./resources/cases.csv", minyear=2016, maxyear=2016, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/cases/cases_2016_per_country.csv"))


# num of deaths
who.main(Namespace(filename="./resources/deaths.csv", minyear=2010, maxyear=2016, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2010-2016_total_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2010, maxyear=2016, numcountries=None, country=None,
                   breakdown=True, fileoutput="./out/deaths/deaths_2010-2016_year_breakdown_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2010, maxyear=2010, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2010_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2011, maxyear=2011, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2011_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2012, maxyear=2012, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2012_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2013, maxyear=2013, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2013_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2014, maxyear=2014, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2014_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2015, maxyear=2015, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2015_per_country.csv"))
who.main(Namespace(filename="./resources/deaths.csv", minyear=2016, maxyear=2016, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/deaths/deaths_2016_per_country.csv"))


# fatality rate
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2010, maxyear=2016, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2010-2016_total_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2010, maxyear=2016, numcountries=None, country=None,
                   breakdown=True, fileoutput="./out/fatality_rate/fatality_rate_2010-2016_year_breakdown_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2010, maxyear=2010, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2010_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2011, maxyear=2011, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2011_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2012, maxyear=2012, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2012_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2013, maxyear=2013, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2013_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2014, maxyear=2014, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2014_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2015, maxyear=2015, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2015_per_country.csv"))
who.main(Namespace(filename="./resources/fatality_rate.csv", minyear=2016, maxyear=2016, numcountries=None, country=None,
                   breakdown=None, fileoutput="./out/fatality_rate/fatality_rate_2016_per_country.csv"))