<?php
switch ($_GET["form"]) {
    case "who":
        $year = $_GET["year"];
        $type = $_GET["type"];
        $path = "downloads/who";
        $rdi = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::KEY_AS_PATHNAME);
        foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {
            if (!is_dir($file) && ($year === "" || strpos($file, $year) !== false) && ($type === "" || strpos($file, $type) !== false)) {
                echo "<a href=$file download target='_blank'>" . basename($file) . "</a><br/>";
            }
        }
        break;

    case "promed":


        break;

    case "graphs":
        $location = $_GET["location"];
        $groupby = $_GET["groupby"];
        $path = "downloads/graphs";
        $rdi = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::KEY_AS_PATHNAME);
        foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {
            if (!is_dir($file) && ($location === "" || strpos(strtolower($file),strtolower($location))) 
            && ($groupby === "" || strpos(strtolower($file),$groupby) !== false)) {
                echo "<a href=$file download target='_blank'>" . basename($file) . "</a><br/>";
            }
        }
        break;
}
?>