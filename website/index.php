
  
<?php include('header.html'); ?>

  


	<div class="jumbotron">
		<h1>A Global Database of Cholera</h1>
		<p style="font-weight: bold;">Big Data to Tackle Infectious Diseases: A Global Database of Cholera Occurrence</p>
	</div>


	<div id="CarouselMaps" class="carousel slide" data-ride="carousel" style="padding: 5%">

		<p style="padding-bottom: 2em;">Maps created with data from WHO repository:</p>
		
		<ol class="carousel-indicators" style="bottom: -1%">
			<li data-target="#CarouselMaps" data-slide-to="0" class="active"></li>
			<li data-target="#CarouselMaps" data-slide-to="1"></li>
			<li data-target="#CarouselMaps" data-slide-to="2"></li>
			<li data-target="#CarouselMaps" data-slide-to="3"></li>
			<li data-target="#CarouselMaps" data-slide-to="4"></li>
			<li data-target="#CarouselMaps" data-slide-to="5"></li>
			<li data-target="#CarouselMaps" data-slide-to="6"></li>
		  </ol>
		
		<div class="carousel-inner" role="listbox" style="margin-bottom: 1%;">
			<div class="carousel-item active">
				<img class="d-block w-100" src="img/cases_2010_per_country.png">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/cases_2011_per_country.png">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/cases_2012_per_country.png">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/cases_2013_per_country.png">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/cases_2014_per_country.png">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/cases_2015_per_country.png">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/cases_2016_per_country.png">
			</div>
		</div>

		<a class="carousel-control-prev" href="#CarouselMaps" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#CarouselMaps" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a>

	</div>


	

	<?php include('footer.html'); ?>


  </body>
</html>

