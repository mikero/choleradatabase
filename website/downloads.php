	<?php include('header.html'); ?>

	<style>
		.scrollable {
			height: 300px;
			overflow-y: scroll;
		}
	</style>


	<div class="container" style="padding-top: 2em; padding-bottom: 2em;">
	<div class="row" style="padding-bottom: 3em">
		<div class="col-md">
			<h1>Data from ProMed</h1>
				<p>
					This file is a collection of anotated and partially processed <a href="https://promedmail.org/">ProMed</a> reports on Cholera. Potentially relevant informationg in the body of the report has been tagged and some metadata has been extracted. For a complete description, please refer to the project documentation.
				</p>
				<a href="downloads/promed/promed.csv" download target='_blank'>promed.csv</a>
				
		</div>
	</div>

		<div class="row">
			<div class="col-md">
				<h1>Data from WHO</h1>
				<p>
					Assembled from the data published on the World Health Organization's <a href="https://www.who.int/gho/database/en/">data repository</a>. Each file covers either one or several years and accounts for all countries where data was collected at any point during that time span.
				</p>

				<form action="search.php" method="get" id="who">
					<label for="year">Year:</label>
					<input type="text" name="year"><br>
					<label for="type">Content:</label>
					<select name="type">
						<option value="">All</option>
						<option value="cases">Cases</option>
						<option value="deaths">Deaths</option>
						<option value="fatality_rate">Fatality rate</option>
					</select><br>
					<input type="submit" name="submit" value="Search" />
					<input type="hidden" name="form" value="who" />
				</form>

				<div class="scrollable" id="who-results">
					<!-- For server results -->
				</div>

			</div>
			
			<div class="col-md">
				<h1>ProMed report graphs</h1>
				<p>
					These graphs show the number of ProMed reports published over time. "Location" can be either a country or "global", which displays the graph for all reports .
				</p>
				<form action="search.php" method="get" id="graphs">
					<label for="location">Location:</label>
					<input type="text" name="location"><br>

					<label for="grouby">Grouped by:</label>


					<select name="groupby">
						<option value="">All</option>
						<option value="month">Month</option>
						<option value="week">Week</option>
					</select><br>

					<input type="submit" name="submit" value="Search" />
					<input type="hidden" name="form" value="graphs" />
				</form>

				<div class="scrollable" id="graphs-results">
					<!-- For server results -->
				</div>

			</div>
		</div>
	</div>





	<script>
		$("#who").submit(request);
		window.onload = $("#who").submit();

		$("#promed").submit(request);
		window.onload = $("#promed").submit();

		$("#graphs").submit(request);
		window.onload = $("#graphs").submit();

		function request(event) {
			event.preventDefault(); //prevent default action 
			var id = $(this).attr("id")
			var post_url = $(this).attr("action"); //get form action url
			var request_method = $(this).attr("method"); //get form GET/POST method
			var form_data = $(this).serialize(); //Encode form elements for submission

			$.ajax({
				url: post_url,
				type: request_method,
				data: form_data
			}).done(function(response) {
				$("#" + id + "-results").html(response);
			});
		}
	</script>




	<!--	<select name="year_dropdown" onchange="dropdown(this.value)" style="margin-bottom: 1em;">
			<option value="" disabled selected hidden>Choose a year</option> 
			<option value="all">All years</option>
			<option value="2010">2010</option>
			<option value="2011">2011</option>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
			<option value="2014">2014</option>
			<option value="2015">2015</option>
			<option value="2016">2016</option>
		</select>
	-->


	<?php include('footer.html'); ?>




	</body>

	</html>