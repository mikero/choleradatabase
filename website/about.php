<?php include('header.html'); ?>

    <section style="padding: 2em">
        <h2>Client</h2>
        <p>
            Luis E. Escobar<br/>
            Assistant Professor<br/>
            Department of Fish and Wildlife Conservation, Virginia Tech<br/>
            Email: escobar1@vt.edu<br/>        
        </p>
        <h2>Project Description</h2>
        <p>
            There is an ongoing pandemic of cholera, which means that this infectious disease has global and simultaneous transmission. Cholera is a bacteria that can kill within hours with an estimated 4.0 million cases that result in ~143,000 deaths annually worldwide.  Dr. Escobar’s laboratory has worked to understand the temporal, spatial, and climatic pulses associated with cholera epidemics. Additionally, the laboratory has been a pioneer in disentangling the potential effects of climate change on this water-borne disease.  A new challenge to understand and prevent this disease is to have a comprehensive assessment of the number of cases at the local level.  
        </p>
        <p>
            Previous work of Dr. Escobar’s laboratory in the study of cholera include:
            <ol>
                <li>
                    Escobar LE, Ryan SJ, Stewart-Ibarra AM, Finkelstein JL, King CA, Qiao H, Polhemus ME. 2015 A global map of suitability for coastal Vibrio cholerae under current and future climate conditions. Acta Trop. 149, 202–211. (doi:10.1016/j.actatropica.2015.05.028)
                </li>
                <li>
                    Watts N et al. 2019 The 2019 report of The Lancet Countdown on health and climate change: Ensuring that the health of a child born today is not defined by a changing climate. Lancet 394, 1836–1878. (doi:10.1016/S0140-6736(19)32596-6)
                </li>
                <li>
                    Ryan SJ et al. 2018 Spatiotemporal variation in environmental Vibrio cholerae in an estuary in southern coastal Ecuador. Int. J. Environ. Res. Public Health 15, 486. (doi:10.3390/ijerph15030486)
                </li>
            </ol>
            <h2>Team members</h2>
                <ul>
                    <li>
                        Hemakshi Sharma, shemak3@vt.edu
                    </li>
                    <li>
                        Gabby Alcantara, gabbyal@vt.edu
                    </li>
                    <li>
                        Michael Roberto, mikero@vt.edu
                    </li>
                    <li>
                        Emily Croxall, emilyc7@vt.edu
                    </li>
                    <li>
                        Andrés García Solares, andresgsol@vt.edu
                    </li>
                </ul>

                <h2>More info</h2>
                <p>
                    The complete documentation for this project can be found at <a href="#">VTechWorks</a>.
                </p>
                <p>
                    The code can be found on our <a href="https://git.cs.vt.edu/mikero/choleradatabase">GitLab repository</a>.
                </p>

        </p>
    </section>


    <?php include('footer.html'); ?>

  </body>
</html>

