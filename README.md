# Cholera Database

A Global Database of Cholera

Big Data to Tackle Infectious Diseases: A Global Database of Cholera Occurrence

**Client**
*  Luis E. Escobar
*  Assistant Professor
*  Department of Fish and Wildlife Conservation, Virginia Tech
*  Phone: 540-232-8454, Email: escobar1@vt.edu

**Team Members**

Gabby Alcantara

Michael Roberto

Emily Croxall

Andrés García Solares

Hemakshi Sharma

**Project Description**

There is an ongoing pandemic of cholera, which means that this infectious disease has global and simultaneous transmission. Cholera is a bacteria that can kill within hours with an estimated 4.0 million cases that result in ~143,000 deaths annually worldwide.  Dr. Escobar’s laboratory has worked to understand the temporal, spatial, and climatic pulses associated with cholera epidemics. Additionally, the laboratory has been a pioneer in disentangling the potential effects of climate change on this water-borne disease.  A new challenge to understand and prevent this disease is to have a comprehensive assessment of the number of cases at the local level.  

Previous work of Dr. Escobar’s laboratory in the study of cholera include:

1. Escobar LE, Ryan SJ, Stewart-Ibarra AM, Finkelstein JL, King CA, Qiao H, Polhemus ME. 2015 A global map of suitability for coastal Vibrio cholerae under current and future climate conditions. Acta Trop. 149, 202–211. (doi:10.1016/j.actatropica.2015.05.028)

2. Watts N et al. 2019 The 2019 report of The Lancet Countdown on health and climate change: Ensuring that the health of a child born today is not defined by a changing climate. Lancet 394, 1836–1878. (doi:10.1016/S0140-6736(19)32596-6)

3. Ryan SJ et al. 2018 Spatiotemporal variation in environmental Vibrio cholerae in an estuary in southern coastal Ecuador. Int. J. Environ. Res. Public Health 15, 486. (doi:10.3390/ijerph15030486)

This project will incorporate digital epidemiology—i.e., the automated collection, curation, storage, and analysis of disease—data obtained from online repositories (e.g., ProMed).  The data will account for the last 10 years. The data from the online repositories will need to be verified for validity and can be used if they come from reliable sources, this includes differentiating outbreaks from stand alone cases. Results from this project will be added to an international effort to inform policy to prevent human mortality around the world. The results will be available to be viewed year-by-year and will be downloadable. 

**Project Deliverables:** A database of cholera cases including location, date, and number of cases where cases are organized by year or location. This project aims to develop a workflow necessary to build a database of cholera cases globally.
 

**Expected impact of the project:** Usage of the database to analyze the data to identify trends and correlations in order to help predict when a cholera outbreak may occur.

**Additional Information**

Escobar: https://fishwild.vt.edu/faculty/escobar.htm (lLinks to an external site.)

The Story of Cholera: https://www.who.int/news-room/fact-sheets/detail/cholera (Links to an external site.)

Similar efforts: https://www.healthmap.org/en/ (Links to an external site.)

Expected use of the data: https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(19)32596-6/fulltext